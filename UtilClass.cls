public with sharing UtilClass{
    public static Boolean isSameMap(Map<String,Object> map1,Map<String,Object> map2){
        Boolean isDiff = true
        for(String key1:map1.keySet()){
	        if(!map2.containsKey(key1) || map2.get(key1) != map1.get(key1)){
	            isDiff = true;
                break;
	        }
        }
    }
}
